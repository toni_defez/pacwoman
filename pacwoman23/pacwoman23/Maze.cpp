#include "Maze.h"
#include "Dot.h"
#include <iostream>
#include <cassert>

Maze::Maze(sf::Texture& texture):m_mazeSize(0,0)
, m_texture(texture)
{
}

//cargador del mapa
void Maze::LoadLevel(std::string name)
{
	/*Tenemos que reiniciar las variables para 
	que no mantenga los datos del nivel anterior*/
	m_mazeSize = sf::Vector2i(0, 0);
	m_mazeData.clear();

	m_pacWomanPosition = sf::Vector2i(0, 0);
	m_ghostPosition.clear();



	sf::Image levelData;
	if (!levelData.loadFromFile("assets/levels/" + name + ".png"))
		throw std::runtime_error("Failed to load level (" + name + ")");
	m_mazeSize = sf::Vector2i(levelData.getSize());

	if (m_mazeSize.x < 15 || m_mazeSize.y < 15)
		throw std::runtime_error("The loaded level is too small (min 15 cells large");

	//lectura pixel a pixel de la imagen
	for (unsigned int y = 0; y < m_mazeSize.y; y++)
	{
		for (unsigned int x = 0; x < m_mazeSize.x; x++)
		{
			sf::Color cellData = levelData.getPixel(x, y);
			
			//En funcion del color se a�ade uno y otro tipo
			// de celda al mapa ( que es un arraylist)
			if (cellData == sf::Color::Black)
				m_mazeData.push_back(Wall);
			else if (cellData == sf::Color::White)
				m_mazeData.push_back(Dot);
			else if (cellData == sf::Color::Green)
				m_mazeData.push_back(SuperDot);
			else if (cellData == sf::Color::Blue)
			{
				//pacwoman starts
				m_pacWomanPosition = sf::Vector2i(x, y);
				m_mazeData.push_back(Empty);
			}
			else
			{
				//ghosts starts
				m_ghostPosition.push_back(sf::Vector2i(x, y));
				m_mazeData.push_back(Empty);
			}
		}
	}
	//Creamos la textura con las correctas dimensiones y la ponemos de negro
	m_renderTexture.create(32 * m_mazeSize.x, 32 * m_mazeSize.y);
	m_renderTexture.clear(sf::Color::Black);

	sf::RectangleShape wall;
	wall.setSize(sf::Vector2f(32, 32));
	wall.setFillColor(sf::Color::Blue);



	//Vamos a crear tres sprites para cada bloque

	sf::Sprite border(m_texture);
	border.setTextureRect(sf::IntRect(16, 0, 16, 32));
	border.setOrigin(0, 16);

	sf::Sprite innerCorner(m_texture);
	innerCorner.setTextureRect(sf::IntRect(0, 0, 16, 16));
	innerCorner.setOrigin(0, 16);

	sf::Sprite outerCorner(m_texture);
	outerCorner.setTextureRect(sf::IntRect(0, 16, 16, 16));
	outerCorner.setOrigin(0, 16);


	m_renderTexture.display();

	for (unsigned int i = 0; i < m_mazeData.size(); i++)
	{
		//el indexToPostion nos dice las coordenadas 
		// donde se encuentra fila 1 columna 2 
		sf::Vector2i position = indexToPosition(i);

		if (isWall(position))
		{
			
			wall.setPosition(32 * position.x, 32 * position.y);
			m_renderTexture.draw(wall);

			//ponemos las diferentes partes que conforman el conjunto del bloque

			border.setPosition(mapCellToPixel(position));
			innerCorner.setPosition(mapCellToPixel(position));
			outerCorner.setPosition(mapCellToPixel(position)); 

			//los bordes rectos 

			/*
			hacia la derecha
			     |-------|-------|
			*/
			if (!isWall(position + sf::Vector2i(1, 0)))
			{
				border.setRotation(0);
				m_renderTexture.draw(border);
			}
			/*
			-
			-
			-
			|------|
			*/
			if (!isWall(position + sf::Vector2i(0, 1)))
			{
				border.setRotation(90);
				m_renderTexture.draw(border);
			}
			/*
			hacia la izquierda
			|-------|-------|
			*/
			if (!isWall(position + sf::Vector2i(-1, 0)))
			{
				border.setRotation(180);
				m_renderTexture.draw(border);
			}
			/*
				|--------|
				-
				-
				-
				-
				_

			*/
			if (!isWall(position + sf::Vector2i(0, -1)))
			{
				border.setRotation(270);
				m_renderTexture.draw(border);
			}


			//los bordes de esquina interiores con todas la posibilidades
			if (isWall(position + sf::Vector2i(1, 0)) && isWall(position + sf::Vector2i(0, -1)))
			{
				innerCorner.setRotation(0);
				m_renderTexture.draw(innerCorner);
			}

			if (isWall(position + sf::Vector2i(0, 1)) && isWall(position + sf::Vector2i(1, 0)))
			{
				innerCorner.setRotation(90);
				m_renderTexture.draw(innerCorner);
			}

			if (isWall(position + sf::Vector2i(-1, 0)) && isWall(position + sf::Vector2i(0, 1)))
			{
				innerCorner.setRotation(180);
				m_renderTexture.draw(innerCorner);
			}

			if (isWall(position + sf::Vector2i(0, -1)) && isWall(position + sf::Vector2i(-1, 0)))
			{
				innerCorner.setRotation(270);
				m_renderTexture.draw(innerCorner);
			}


			//los bordes de esquina exteriores con todas la posibilidades
			if (!isWall(position + sf::Vector2i(1, 0)) && !isWall(position + sf::Vector2i(0, -1)))
			{
				outerCorner.setRotation(0);
				m_renderTexture.draw(outerCorner);
			}

			if (!isWall(position + sf::Vector2i(0, 1)) && !isWall(position + sf::Vector2i(1, 0)))
			{
				outerCorner.setRotation(90);
				m_renderTexture.draw(outerCorner);
			}

			if (!isWall(position + sf::Vector2i(-1, 0)) && !isWall(position + sf::Vector2i(0, 1)))
			{
				outerCorner.setRotation(180);
				m_renderTexture.draw(outerCorner);
			}

			if (!isWall(position + sf::Vector2i(0, -1)) && !isWall(position + sf::Vector2i(-1, 0)))
			{
				outerCorner.setRotation(270);
				m_renderTexture.draw(outerCorner);
			}
		}
	}
	
}

sf::Vector2i Maze::getPacwomanPostion() const
{
	return m_pacWomanPosition;
}

std::vector<sf::Vector2i> Maze::getGhostPosition() const
{
	return m_ghostPosition;
}

inline std::size_t Maze::positionToIndex(sf::Vector2i position) const
{
	return position.y * m_mazeSize.x + position.x;
}

inline sf::Vector2i Maze::indexToPosition(std::size_t index) const
{
	sf::Vector2i position;

	position.x = index % m_mazeSize.x;
	position.y = index / m_mazeSize.x;

	return position;
}

sf::Vector2i Maze::mapPixelToCell(sf::Vector2f pixel) const
{
	sf::Vector2i cell;
	cell.x = std::floor(pixel.x / 32.f);
	cell.y = std::floor(pixel.y / 32.f);

	return cell;
}

sf::Vector2f Maze::mapCellToPixel(sf::Vector2i cell) const
{
	sf::Vector2f pixel;
	pixel.x = cell.x * 32 + 16;
	pixel.y = cell.y * 32 + 16;
	return pixel;
}

/*
Se encarga de dibujar los dots y superdot del mapa
son el unico elemento (respecto al mapa que cambia)
*/
void Maze::draw(sf::RenderTarget & target, sf::RenderStates states) const
{ 
	target.draw(sf::Sprite(m_renderTexture.getTexture()), states);
	static sf::CircleShape dot = getDot();
	static sf::CircleShape superDot = getSuperDot();

	for (unsigned int i = 0; i < m_mazeData.size(); i++)
	{
		sf::Vector2i position = indexToPosition(i);

		if (m_mazeData[i] == Dot)
		{
			dot.setPosition(32 * position.x + 16, 32 * position.y + 16);
			target.draw(dot, states);
		}
		else if (m_mazeData[i] == SuperDot)
		{
			superDot.setPosition(32 * position.x + 16, 32 * position.y + 16);
			target.draw(superDot, states);
		}
	} 
}

/*Comparamos si la posicion que nos pasan esta ocupada por  
un muro*/
bool Maze::isWall(sf::Vector2i position) const
{ 
	if (position.x < 0 || position.y < 0 || position.x >= m_mazeSize.x || position.y >= m_mazeSize.y)
		return false;
	return m_mazeData[positionToIndex(position)]==Wall;   
}

bool Maze::isDot(sf::Vector2i position) const
{
	return m_mazeData[positionToIndex(position)] == Dot;
}

bool Maze::isSuperDot(sf::Vector2i position) const
{
	return m_mazeData[positionToIndex(position)] == SuperDot;
}

void Maze::pickObject(sf::Vector2i position) 
{
	
 	assert(!isWall(position)); //estamos asegurando de que no es un  muro
	m_mazeData[positionToIndex(position)] = Empty;
}

bool Maze::isBonus(sf::Vector2i position) const
{
	return m_mazeData[positionToIndex(position)] == Bonus; 
}

sf::Vector2i Maze::getSize() const
{
	return m_mazeSize;
}

int Maze::getRemainingDots() const
{
	int remaingDots = 0;
	for (unsigned int i = 0; i <m_mazeData.size();i++)

		if(m_mazeData[i]==Dot || m_mazeData[i]==SuperDot)
			remaingDots++;

	return remaingDots;
}

Maze::~Maze()
{
}

