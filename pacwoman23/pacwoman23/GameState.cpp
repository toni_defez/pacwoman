#include "GameState.h"
#include "Game.h"
#include <vector>

#include<algorithm>
/*************/
//Esta funcion sirve para poner cualquier elemento
//su punto de origen en el centro

template <typename T>
void centerOrigin(T& drawable)
{
	sf::FloatRect bound = drawable.getLocalBounds();
	drawable.setOrigin(bound.width / 2, bound.height / 2);
}
/***************/


GameState::GameState(Game* game)
	:m_game(game)
{

}

Game * GameState::getGame() const
{
	return m_game;
}


GameState::~GameState()
{
}

void GameState::insertCoin()
{
}

void GameState::pressButton()
{
}

void GameState::moveStick(sf::Vector2i direction)
{
}

void GameState::Update(sf::Time delta)
{
}

void GameState::Draw(sf::RenderWindow & window)
{
}


/************************************************/
/*Estados*/
NoCoinState::NoCoinState(Game* game) :GameState(game)
{
	m_sprite.setTexture(game->getLogo());
	m_sprite.setPosition(20, 50);
	m_text.setFont(game->getFont());
	m_text.setString("Insert coin");
	centerOrigin(m_text);
	m_text.setPosition(240, 150);
	display_text = true;
}
void NoCoinState::insertCoin()
{
	//pulsamos  i y nos vamos a la pantalla de 
	//de esta manera pasamos a otro estado
	//
	getGame()->ChangeGameState(GameState::GetReady);
}
void NoCoinState::pressButton()
{

}

void NoCoinState::moveStick(sf::Vector2i direction)
{
	std::cout << "Hola" << std::endl;
}

void NoCoinState::Update(sf::Time delta)
{
	/*En update las cosas van asi
	tenemos una variable de tiempo que indicara
	cuando tenemos que dibujar el texto*/
	static sf::Time timeBuffer = sf::Time::Zero;
	timeBuffer += delta;

	while (timeBuffer >= sf::seconds(0.5))
	{
		display_text = !display_text;
		timeBuffer -= sf::seconds(1);
	}
}

void NoCoinState::Draw(sf::RenderWindow& window)
{
	window.draw(m_sprite);
	/*Con esto conseguimos que el texto parpade
	mirar update*/
	if (display_text)
		window.draw(m_text);
}

/********************/
GetReadyState::GetReadyState(Game * game, GameState* playing) :GameState(game)
, m_playingState(playing)
{
	m_text.setFont(game->getFont());
	m_text.setString("Are you ready?");
	m_text.setCharacterSize(14);
	centerOrigin(m_text);
	m_text.setPosition(250, 205);
}

void GetReadyState::insertCoin()
{
}

void GetReadyState::pressButton()
{
	getGame()->ChangeGameState(GameState::Playing);
}

void GetReadyState::moveStick(sf::Vector2i direction)
{
}

void GetReadyState::Update(sf::Time delta)
{
	m_playingState->Update(delta);
}

void GetReadyState::Draw(sf::RenderWindow & window)
{
	m_playingState->Draw(window);
	window.draw(m_text);
}
/*******************************/
PlayingState::PlayingState(Game * game) :GameState(game)
, m_pacWoman(nullptr)
//, m_ghost(game->getTexture())
, m_Maze(game->getTexture())
, m_level(0),
m_liveCount(3),
m_score(0)
{



	//creacion de pacwoman
	m_pacWoman = new PacWoman(game->getTexture());
	//asociamos el mapa con pacwoman
	m_pacWoman->setMaze(&m_Maze);
	/*Y colocamos a pacwoman en su posicion*/
	m_pacWoman->setPosition(m_Maze.mapCellToPixel(m_Maze.getPacwomanPostion()));

	resetToZero();
	//la camara va ha tener un  tama�o menor que la pantalla original(hay que incluir el hub) y 
	//estara apuntando siempre a la posicion de pacwoman y eso lo hacemos en el update
	m_camera.setSize(sf::Vector2f(480, 480));
	//para el hub
	m_scene.create(480, 480);

	//iniciamos los componenetes de texto del hub
	m_scoreText.setFont(game->getFont());
	m_scoreText.setCharacterSize(10);
	m_scoreText.setPosition(10, 480);


	m_levelText.setFont(game->getFont());
	m_levelText.setCharacterSize(10);
	m_levelText.setPosition(160, 480);


	m_remainingDotsText.setFont(game->getFont());
	m_remainingDotsText.setCharacterSize(10);
	m_remainingDotsText.setPosition(270, 480);


	//colocando las vidas
	for (auto& m_livesSprite : m_livesSprite)
	{
		m_livesSprite.setTexture(game->getTexture());
		m_livesSprite.setTextureRect(sf::IntRect(122, 0, 20, 20));

	}

	m_livesSprite[0].setPosition(sf::Vector2f(415, 480));
	m_livesSprite[1].setPosition(sf::Vector2f(435, 480));
	m_livesSprite[2].setPosition(sf::Vector2f(455, 480));
}

void PlayingState::insertCoin()
{
	//m_pacWoman->Die();
}

void PlayingState::pressButton()
{
	//m_ghost.setWeak(sf::seconds(3));
}

void PlayingState::moveStick(sf::Vector2i direction)
{
	/*Pruebas para el cambio de navegacion
	lo haremos asi
	Cuando pulse derecha ire a win
	Cuando pulse izquierda ire a loose
	*/

	m_pacWoman->setDirection(direction);
	/*
	if (direction.x == -1)
	getGame()->ChangeGameState(GameState::Lost);
	else if (direction.x == 1)
	getGame()->ChangeGameState(GameState::Won);
	*/
}

void PlayingState::Update(sf::Time delta)
{

	m_pacWoman->update(delta);
	for (Ghost* ghost : m_ghosts)
		ghost->Update(delta);

	//para las colisiones
	sf::Vector2f pixelPosition = m_pacWoman->getPosition();
	sf::Vector2f offset(std::fmod(pixelPosition.x, 32), std::fmod(pixelPosition.y, 32));
	offset -= sf::Vector2f(16, 16);

	//colisiones con objetos
	if (offset.x <= 2 && offset.x >= -2 && offset.y <= 2 && offset.y >= -2)
	{

		sf::Vector2i cellPosition = m_Maze.mapPixelToCell(pixelPosition);
		//si es dot
		if (m_Maze.isDot(cellPosition))
		{
			m_score += 5;
		}
		//si es superdot
		else if (m_Maze.isSuperDot(cellPosition))
		{
			for (Ghost* ghost : m_ghosts)
			{
				//debilitamos a los fantasmas
				ghost->setWeak(sf::seconds(5));
			}
			m_score += 25;
		}
		else if (m_Maze.isBonus(cellPosition))
		{
			m_score += 500;
		}
		//vaciamos la celda
		m_Maze.pickObject(cellPosition);
	}

	bool actualdelete = false;
	int size = m_ghosts.size();
	for (int i = 0; i<size && !actualdelete; i++)
	{
		if (m_ghosts[i]->getCollisionBox().intersects(m_pacWoman->getCollisionBox()))
		{
			if (m_ghosts[i]->isWeak())
			{
				/**/
				m_ghosts.erase(
					std::remove(m_ghosts.begin(), m_ghosts.end(), m_ghosts[i])
					, m_ghosts.end()
				);
				std::cout << "Fantasma muerto" << std::endl;
				m_score += 100;
				actualdelete = true;
			}
			else
				m_pacWoman->Die();
		}
	}



	//si pacwoman muere+
	if (m_pacWoman->isDead())
	{
		m_pacWoman->reset();
		m_liveCount--;

		if (m_liveCount<0)
			getGame()->ChangeGameState(GameState::Lost);
		else
		{
			moveCharactersToInitialPosition();
		}

	}

	updateCamera();

	if (m_Maze.getRemainingDots() == 0)
		getGame()->ChangeGameState(GameState::Won);

	//update scoreText
	m_scoreText.setString(" "+std::to_string(m_score) + " points");
	//update RemianingDots
	m_remainingDotsText.setString(std::to_string(m_Maze.getRemainingDots()) + "x dots ");

}




void PlayingState::Draw(sf::RenderWindow & window)
{
	m_scene.clear();
	//window.draw(m_text);
	m_scene.setView(m_camera);
	m_scene.draw(m_Maze);
	for (Ghost* ghost : m_ghosts)
		m_scene.draw(*ghost);

	m_scene.draw(*m_pacWoman);

	m_scene.display();

	window.draw(sf::Sprite(m_scene.getTexture()));

	//dibujando los componentes
	window.draw(m_scoreText);
	window.draw(m_levelText);
	window.draw(m_remainingDotsText);


	for (unsigned int i = 0; i < m_liveCount; i++)
	{
		window.draw(m_livesSprite[i]);
	}
}

//es el encargado de cargar los mapas en funcion 
//del nivel en que nos encontremos
void PlayingState::loadNextLevel()
{
	//En el constructor  ponemos al loadLevel ya que solo se va a dibujar
	//una vez ---> son los muros ni los fantasmas ni pacwoman ni docs

	m_level++;

	int mapLevel = m_level % 3;
	int speedLevel = std::floor(m_level / 3); //redondeo

	if (mapLevel == 0)
		m_Maze.LoadLevel("small");
	else if (mapLevel == 1)
		m_Maze.LoadLevel("medium");
	else if (mapLevel == 2)
		m_Maze.LoadLevel("large");

	//Tenemos que destruir lo anterior

	for (Ghost* ghost : m_ghosts)
		delete ghost;


	//despues de crear los niveles
	//tenemos que aumentar la velocidad de los personajes 
	//asi como colocarlos en su nueva posicion
	//creacion de fantasmas y posicionamiento de los mismos
	for (auto ghostPostion : m_Maze.getGhostPosition())
	{
		Ghost* ghost = new Ghost(getGame()->getTexture(), m_pacWoman);
		ghost->setMaze(&m_Maze);
		ghost->setPosition(m_Maze.mapCellToPixel(ghostPostion));

		m_ghosts.push_back(ghost);
	}

	float speed = 100 + (speedLevel + 50);

	m_pacWoman->setSpeed(speed + 25);

	for (auto& ghost : m_ghosts)
		ghost->setSpeed(speed);

	moveCharactersToInitialPosition();

	m_levelText.setString("level " + std::to_string(speedLevel) + " - " + std::to_string(mapLevel + 1)+"");
}

void PlayingState::resetToZero()
{
	resetLiveCount();
	m_level = 0;
	m_score = 0;
	resetCurrentLevel();
}

void PlayingState::resetCurrentLevel()
{
	std::cout << m_level << std::endl;
	m_level--;
	loadNextLevel();

}

void PlayingState::resetLiveCount()
{
	m_liveCount = 3;
}



void PlayingState::updateCamera()
{
	m_camera.setCenter(m_pacWoman->getPosition());
	//centrando la imagen cuando se encuentre por el borde 
	if (m_camera.getCenter().x < 240)
		m_camera.setCenter(240, m_camera.getCenter().y);
	if (m_camera.getCenter().y < 240)
		m_camera.setCenter(m_camera.getCenter().x, 240);
	//el getSize solo nos da el numero de bloque
	//para obtener en pixeles multiplicamos por 32
	if (m_camera.getCenter().x > m_Maze.getSize().x * 32 - 240)
		m_camera.setCenter(m_Maze.getSize().x * 32 - 240, m_camera.getCenter().y);

	if (m_camera.getCenter().y > m_Maze.getSize().x * 32 - 240)
		m_camera.setCenter(m_camera.getCenter().x, m_Maze.getSize().y * 32 - 240);
}

void PlayingState::moveCharactersToInitialPosition()
{
	m_pacWoman->setPosition(m_Maze.mapCellToPixel(m_Maze.getPacwomanPostion()));

	auto ghostPositions = m_Maze.getGhostPosition();
	for (unsigned int i = 0; i < m_ghosts.size()-1; i++)
		m_ghosts[i]->setPosition(m_Maze.mapCellToPixel(ghostPositions[i]));

	updateCamera();
}



PlayingState::~PlayingState()
{
	delete m_pacWoman;
	for (Ghost* ghost : m_ghosts)
	{
		delete ghost;
	}
}

/******************/
LostState::LostState(Game * game, GameState* playing) :GameState(game)
, m_playingState(static_cast<PlayingState*>(playing))
{
	m_text.setFont(game->getFont());
	m_text.setString("Looser!!");
	m_text.setCharacterSize(42);
	centerOrigin(m_text);
	m_text.setPosition(240, 120);

	m_countDownText.setFont(game->getFont());
	m_countDownText.setCharacterSize(12);
	m_countDownText.setPosition(240, 240);

}

void LostState::insertCoin()
{
	//si el jugador inserta una moneda volveremos al nivel actual
	m_playingState->resetCurrentLevel();
	m_playingState->resetLiveCount();
	//volvemos a poner la maquina en estado finito
	getGame()->ChangeGameState(GameState::GetReady);
}

void LostState::pressButton()
{
}

void LostState::moveStick(sf::Vector2i direction)
{
}

void LostState::Update(sf::Time delta)
{
	/*
	Este update lo que hace es contar los
	siguientes 10 segundos para cambiar a la pantalla
	de inicio
	*/

	m_countDown += delta;
	if (m_countDown.asSeconds() >= 10)
	{
		m_playingState->resetToZero();//reinicio del juego
		getGame()->ChangeGameState(GameState::GetReady);
	}
	m_countDownText.setString("Insert a coin to continue..."
		+ std::to_string(10 - static_cast<int>(m_countDown.asSeconds())));
}

void LostState::Draw(sf::RenderWindow & window)
{
	window.draw(m_text);
	window.draw(m_countDownText);
}
/******************/
WonState::WonState(Game * game, GameState* playing) :GameState(game),
m_playingState(static_cast<PlayingState*>(playing))
{
	m_text.setFont(game->getFont());
	m_text.setString("Winner!!");
	m_text.setCharacterSize(42);
	centerOrigin(m_text);
	m_text.setPosition(240, 120);
}

void WonState::insertCoin()
{
}

void WonState::pressButton()
{
}

void WonState::moveStick(sf::Vector2i direction)
{
}

void WonState::Update(sf::Time delta)
{
	//Aqui incrementamos el time buffer hasta
	//que llegue a cinco en ese punto lo que haremos
	//sera cambiar de pantalla a la de ready

	static sf::Time timeBuffer = sf::Time::Zero;
	timeBuffer += delta;
	if (timeBuffer.asSeconds() > 5)
	{
		m_playingState->loadNextLevel(); //pasamos al siguiente nivel
		getGame()->ChangeGameState(GameState::GetReady);
	}
}

void WonState::Draw(sf::RenderWindow & window)
{
	window.draw(m_text);
}