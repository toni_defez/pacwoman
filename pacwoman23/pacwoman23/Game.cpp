#include "Game.h"


/*Puedo hacer esto en c++
definir una variable antes de meterme
en el constructor*/

Game::Game():m_window(sf::VideoMode(480, 500), "PacWoman")
{
	//Inicializamos el resto de elementos
	if (!m_font.loadFromFile("assets/font.ttf"))
	{
		//lanzando una excepcion  con mensaje personalizado
		throw std::runtime_error("Unable to load the font file");
	}

	if (!m_logo.loadFromFile("assets/logo.png"))
	{
		//lanzando excepcion runtime_error con mensaje personalizado
		throw std::runtime_error("Unable to load the logo file");
	}

	if (!m_texture.loadFromFile("assets/texture.png"))
		throw std::runtime_error("Unable to load the texture file");


	//Inicializamos los estados
	m_gameStates[GameState::NoCoin] = new NoCoinState(this);
	m_gameStates[GameState::Playing] = new PlayingState(this);
	m_gameStates[GameState::GetReady] = new GetReadyState(this, m_gameStates[GameState::Playing]);
	
	m_gameStates[GameState::Lost] = new LostState(this, m_gameStates[GameState::Playing]);
	m_gameStates[GameState::Won] = new WonState(this, m_gameStates[GameState::Playing]);
	

	//el estado inicial va a ser el estado noCoin
	ChangeGameState(GameState::NoCoin);
}


//El destructor de la clase 
Game::~Game()
{
	for (GameState* gameState : m_gameStates)
		delete gameState;

}

void Game::Run()
{
	sf::Clock frameclock;
	while (m_window.isOpen())
	{
		sf::Event event;
		//manejador de eventos
		while (m_window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				m_window.close();

			//controlando  las pulsaciones de teclado
			//con el evento 

			if (event.type == sf::Event::KeyPressed)
			{
				if (event.key.code == sf::Keyboard::I)
					m_currentState->insertCoin();
				if (event.key.code == sf::Keyboard::S)
					m_currentState->pressButton();
				//Moviendo al personaje
				if (event.key.code == sf::Keyboard::Left)
					m_currentState->moveStick(sf::Vector2i(-1, 0));

				if (event.key.code == sf::Keyboard::Right)
					m_currentState->moveStick(sf::Vector2i(1,0));

				if (event.key.code == sf::Keyboard::Up)
					m_currentState->moveStick(sf::Vector2i(0, -1));

				if (event.key.code == sf::Keyboard::Down)
					m_currentState->moveStick(sf::Vector2i(0, 1));
			}
		}
		/*Para cada estado mostrara
		su upadte, su draw y demas cosas
		*/
		m_currentState->Update(frameclock.restart());
		m_window.clear();
		m_currentState->Draw(m_window);
		m_window.display();
		
	}
}


void Game::ChangeGameState(GameState::State gameState)
{
	m_currentState = m_gameStates[gameState];
}


sf::Font& Game::getFont()
{
	return m_font;
}

sf::Texture& Game::getTexture()
{
	return m_texture;
}

sf::Texture& Game::getLogo()
{
	return m_logo;
}