#pragma once
#ifndef PACWOMAN_CHARACTER_HPP
#define PACWOMAN_CHARACTER_HPP
#include <SFML\Graphics.hpp> 
#include "Maze.h"
/*De esta clase heredaran las clase Ghost 
  y PacWoman implementara las funcionalidades
  basicas
*/

/*Se incluye el maze ya que es este el que definira las posiciones
de inicio de los fantasmas asi como el del personaje incial*/
class Character:public sf::Drawable, public sf::Transformable
{
public:
	Character();
	~Character();  
	virtual void update(sf::Time delta);
	void setDirection(sf::Vector2i direction);
	sf::Vector2i getDirection() const;

	void setSpeed(float speed);
	float getSpeed() const;  

	void setMaze(Maze* maze);
	bool willMove ();
	//metodo para las colicioens
	sf::FloatRect getCollisionBox()const;



protected:
	virtual void changeDirection() {};

private:
	float m_speed;
	Maze* m_maze;
	sf::Vector2i m_currentDirection;
	sf::Vector2i m_nextDirection;
	
	sf::Vector2i m_previousIntersection;
	std::array<bool, 4> m_availableDirections;
} ;

#endif //PACWOMAN_CHARACTER_HPP

