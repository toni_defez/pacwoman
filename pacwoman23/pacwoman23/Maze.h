#ifndef PACWOMAN_MAZE_HPP
#define PACWOMAN_MAZE_HPP

#include <SFML\Graphics.hpp>
#include <array>
/*El laberinto de pacwoman
Esta clase se basa en el princpio de crear
el mapa a traves de la lectura byte a byte de
una imagen
segun esto los bloques blancos es donde se puede traspasar
los negros son barreras fisicas
los rojos son la posicion inicial del los fantasmas
el azul(solo habra uno en pantalla) sera la posicion inicial de pacwoman
y los verdes seran donde apareceran los superdot(puntos de bonificacion extra)


*/
class Maze : public sf::Drawable
{
public:
	Maze(sf::Texture& texture );
	void LoadLevel(std::string name);
	//Obtener posiciones originales del pacwoman y ghost
	sf::Vector2i getPacwomanPostion() const;
	std::vector<sf::Vector2i> getGhostPosition() const;

	//las hacemos inline para que vayan mas rapido
	inline std::size_t positionToIndex(sf::Vector2i position) const;
	inline sf::Vector2i indexToPosition(std::size_t index) const;

	//posicion inicial de pacwoman
	sf::Vector2i mapPixelToCell(sf::Vector2f pixel) const;
	sf::Vector2f mapCellToPixel(sf::Vector2i cell) const;

	//comprobar si es muro 
	bool isWall(sf::Vector2i position)const;
	
	bool isDot(sf::Vector2i position)const;
	bool isSuperDot(sf::Vector2i position)const;
	void pickObject(sf::Vector2i position);
	bool isBonus(sf::Vector2i position)const;
	//void Reset

	//para saber sus dimensiones para delimitar la camara
	sf::Vector2i getSize() const;

	int getRemainingDots()const;

	~Maze();
private:
	//los diferentes tipos de bloque
	//vacios,muro
	enum CellData
	{
		  Empty,
		  Wall,
		  Dot,
		  SuperDot,
		  Bonus
	};
	//sobrescritura del metodo de draw
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	//posiciones de pacwoman asi como de los diferentes fantasmas
	sf::Vector2i m_pacWomanPosition;
	std::vector <sf::Vector2i> m_ghostPosition;

	//variables descriptivas de las variables del mapa
	sf::Vector2i m_mazeSize;
	std::vector<CellData> m_mazeData;
	sf::RenderTexture m_renderTexture;

	//vamos a acceder a la textura para cambiar el aspecto del laberinto
	sf::Texture& m_texture;
}; 
#endif //PACWOMAN_MAZE_HPP

