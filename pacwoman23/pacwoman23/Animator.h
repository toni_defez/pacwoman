#ifndef PACWOMAN_ANIMATOR_HPP
#define PACWOMAN_ANIMATOR_HPP
#include <SFML/Graphics.hpp>

/*Esta clase es la encargada de la animaciones
de los elementos del juego
	Esta compuesta por un vector de sprites
	asi como varios metodos para a�adir ,actualizar frames
*/

class Animator
{
public:
	Animator();
	~Animator();
	void AddFrame(sf::IntRect frame);
	/*Inicializa la animacion*/
	void Play(sf::Time duration, bool loop);
	/*Variable de control*/
	bool IsPlaying()const;

	void Update(sf::Time delta);
	void Animate(sf::Sprite& sprite);

private:
	/*Este es el vector donde a�adiremos 
	los sprites*/
	std::vector <sf::IntRect> m_frames;
	bool m_isPlaying;
	sf::Time m_duration;
	bool m_loop;
	unsigned int m_currentFrame; 
};

#endif //PACWOMAN_ANIMATOR_HPP