#include "PacWoman.h"

PacWoman::PacWoman(sf::Texture& texture)
	:m_visual(texture)
	, m_isDead(false)
	, m_isDying(false)
{
	setOrigin(20, 20);

	//a�adiendo los frames para las animaciones
	//para correr
	m_runAnimator.AddFrame(sf::IntRect(0, 32, 40, 40));
	m_runAnimator.AddFrame(sf::IntRect(0, 72, 40, 40));

	//para morir
	m_dieAnimator.AddFrame(sf::IntRect(0, 32, 40, 40));
	m_dieAnimator.AddFrame(sf::IntRect(0, 72, 40, 40));
	m_dieAnimator.AddFrame(sf::IntRect(0, 112, 40, 40));
	m_dieAnimator.AddFrame(sf::IntRect(40, 112, 40, 40));
	m_dieAnimator.AddFrame(sf::IntRect(80, 112, 40, 40));
	m_dieAnimator.AddFrame(sf::IntRect(120, 112, 40, 40));
	m_dieAnimator.AddFrame(sf::IntRect(160, 112, 40, 40));
	
	//inicializamos la animacion de correr
	m_runAnimator.Play(sf::seconds(0.25), true);
	
}



PacWoman::~PacWoman()
{
}

/*Se encarga de poner 
la varialbe de muerte activada
*/
void PacWoman::Die()
{
	if (!m_isDying)
	{
		m_dieAnimator.Play(sf::seconds(0.5),false);
		m_isDying = true;
	}
	
}

/*No se que quiere decir esta funcion*/
bool PacWoman::isDead() const
{
	return m_isDead;
}

bool PacWoman::isDying() const
{
	return m_isDying;
}

void PacWoman::update(sf::Time delta)
{
	if (!m_isDead && !m_isDying)
	{
		m_runAnimator.Update(delta);
		m_runAnimator.Animate(m_visual);
	}
	else
	{
		m_dieAnimator.Update(delta);
		m_dieAnimator.Animate(m_visual);

		if (!m_dieAnimator.IsPlaying())
		{
			m_isDying = false;
			m_isDead = true;
		}
	}
	Character::update(delta);
}

//reseteamos todos los valores de pacwoman
void PacWoman::reset()
{
	m_isDead = false;
	m_isDying = false;
	
	m_runAnimator.Play(sf::seconds(0.25), true);
	m_runAnimator.Animate(m_visual);
}

void PacWoman::draw(sf::RenderTarget& target, sf::RenderStates states)const
{
	states.transform *= getTransform();
	if (!m_isDead)
	{
		target.draw(m_visual, states);
	}
}