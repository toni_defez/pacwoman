 #ifndef PACWOMAN_PACWOMAN_HPP
#define PACWOMAN_PACWOMAN_HPP
#include "Character.h"
#include "Animator.h"

class PacWoman :
	public Character
{
public:
	PacWoman(sf::Texture& texture);
	~PacWoman();
	void Die();

	bool isDead() const; 
	bool isDying() const;

	void update(sf::Time delta);

	void reset();

private:
	void draw(sf::RenderTarget& target, sf::RenderStates states)const;
	sf::Sprite m_visual;

	bool m_isDead;
	bool m_isDying;//bool para activar al animacion de muerte

	Animator m_runAnimator;
	Animator m_dieAnimator;


};
#endif //PACWOMAN_PACWOMAN_HPP

