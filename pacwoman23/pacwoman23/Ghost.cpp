#include "Ghost.h"



void Ghost::setWeak(sf::Time duration)
{
	m_isWeak = true;
	m_weaknessDuration = duration;
}

bool Ghost::isWeak() const
{
	return m_isWeak;
}

Ghost::Ghost(sf::Texture & texture, PacWoman* pacWoman) : m_visual(texture),
m_isWeak(false),m_weaknessDuration(sf::Time::Zero), m_pacWoman(pacWoman)
{
	setOrigin(20, 20);
	//inicializamos las animaciones
	//animacion de fuerte
	m_strongAnimator.AddFrame(sf::IntRect(40, 32, 40, 40));
	m_strongAnimator.AddFrame(sf::IntRect(80, 32, 40, 40));
	//animacion de debil
	m_weakAnimator.AddFrame(sf::IntRect(40, 72, 40, 40));
	m_weakAnimator.AddFrame(sf::IntRect(80, 72, 40, 40));

	m_strongAnimator.Play(sf::seconds(0.25), true);
	m_weakAnimator.Play(sf::seconds(1), true);
}

Ghost::~Ghost()
{
}

//el update decide que animacion se va a producir
void Ghost::Update(sf::Time delta)
{
	if (m_isWeak)
	{
		m_weaknessDuration -= delta;
		if (m_weaknessDuration <= sf::Time::Zero)
		{
			m_isWeak = false;
			m_strongAnimator.Play(sf::seconds(0.25), true);
		}
	}

	if (!m_isWeak)
	{
		m_strongAnimator.Update(delta);
		m_strongAnimator.Animate(m_visual);
	}
	else
	{
		m_weakAnimator.Update(delta);
		m_weakAnimator.Animate(m_visual);
	}
	Character::update(delta);
}


void Ghost::changeDirection()
{
	//el primer paso va en organizar la lista de direcciones
	//en funcion de cual es mas optima o menos para acercarse a pacwoman

	//este contendor guarda los angulos que hay entre los 
	//distintas direcciones

	//LISTAS DE MOVIMIENTOS

	static sf::Vector2i directions[4] = {
		sf::Vector2i(1, 0),
		sf::Vector2i(0, 1),
		sf::Vector2i(-1, 0),
		sf::Vector2i(0, -1)
	};


	std::map<float, sf::Vector2i> directionProb;
	float targetAngle;

	//sacamos el vector director, restando ambas coordenadas
	sf::Vector2f distance = m_pacWoman->getPosition() - getPosition();

	//sacamos la direccion que objetivo y lo pasamos a grados
	targetAngle = std::atan2(distance.x, distance.y)* (180/3.14);

	//recorremos todas la direcciones para buscar la mas optima
	for (auto direction : directions)
	{
		float directionAngle = std::atan2(direction.x, direction.y)*(180 / 3.14);

		//tenemos que normalizar el angulo, porque puede haber diferntes 
		//grados para mismos angulos--->-90 270
		//obtenemos el valor absoluto y le restamos 180 para que este dentro del 
		//rango
		float diff = 180 - std::abs(std::abs(directionAngle - targetAngle)-180);
		
		directionProb[diff] = direction;
		//de esta manera ordenamos las direcciones en funcion de diff
		//ordenandolas de forma automatica el map de menor(mas optima) a peor(menos optima)
	}
	//sacamos la mejor direccion pero esto no siempre tiene que ser asi puede estar bloqueada
	//usamos la segunda porque la primera posicion esta ocupada por la direccion actual
	setDirection(directionProb.begin()->second);

	//vamos a recorrer todas las direccion hasta que haya una disponible
	auto it = directionProb.begin();
	do
	{
		setDirection(it->second);
		it++;
	} while (!willMove());

}

void Ghost::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	states.transform *= getTransform();
	target.draw(m_visual, states);
}
