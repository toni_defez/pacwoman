#include "Dot.h"

sf::CircleShape getDot()
{
	sf::CircleShape dot;
	dot.setRadius(4);
	dot.setOrigin(2, 2);
	return dot;
}

sf::CircleShape  getSuperDot()
{
	sf::CircleShape superdot;
	superdot.setRadius(8);
	superdot.setOrigin(4, 4);
	return superdot;
}

