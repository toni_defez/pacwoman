#include "Animator.h"



/*
Cuando inciamos el animtor
no tiene ningun frame dentro
y su duracion no esta definida*/
Animator::Animator()
	:m_currentFrame(0),
	m_isPlaying(false),
	m_duration(sf::Time::Zero),
	m_loop(false)
{
}


Animator::~Animator()
{
}

/*Lo primero que tenemos que hacer
cuando usamos un animator es llenarlo
de informacion(frames)s*/
void Animator::AddFrame(sf::IntRect frame)
{
	m_frames.push_back(frame);
}

/*
Para iniciar un animator lo unico
que tenemos que hacer es actualizar
los valores de la animacion
*/
void Animator::Play(sf::Time duration, bool loop)
{
	m_isPlaying = true;
	m_duration = duration;
	m_loop = loop;
}

bool Animator::IsPlaying() const
{
	return m_isPlaying;
}

/*El metodo update es el mas importante
de todos los metodos
*/
void Animator::Update(sf::Time delta)
{
	if (!IsPlaying())
		return;

	static sf::Time timeBuffer = sf::Time::Zero;
	timeBuffer += delta;

	sf::Time frameDuration = m_duration / static_cast<float>(m_frames.size());

	while (timeBuffer > frameDuration)
	{
		m_currentFrame++;

		if (m_currentFrame == m_frames.size())
		{
			if (!m_loop)
				m_isPlaying = false;

			m_currentFrame = 0;
		}

		timeBuffer -= frameDuration;
	}

}

/*cambia la textura del sprite visible*/
void Animator::Animate(sf::Sprite & sprite)
{
	sprite.setTextureRect(m_frames[m_currentFrame]);
}
