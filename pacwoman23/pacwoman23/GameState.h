#ifndef PACWOMAN_GAMESTATE_HPP
#define PACWOMAN_GAMESTATE_HPP
/*Lo de las cabeceras se usan para mejorar
el timempo de compilacion 
Se ponen unicamente en los archivos de cabecera
*/
/*
El gameState engloba las clase generica de la clase
asi como los diferentes estados en que podemos encontrar
en el juego.
En un principio definir varias clases en un unico h.

*/

#include "SFML\Graphics.hpp"
#include <iostream>
#include "Ghost.h"
#include "PacWoman.h"
#include "Maze.h"
class Game;

class GameState
{
public:
	/*El constructor necesita un juego
	padre*/
	GameState(Game* game);
	/*Dar el juego padre*/
	Game* getGame() const;
	~GameState();
	/*El tipo enum indica los diferentes
	estados que nos podremos encontrar 
	en el juego*/
	enum State
	{
		NoCoin,
		GetReady,
		Playing,
		Won,
		Lost,
		Count  //Con este valor conseguimos
			   //la cantidad de elementos que hay
			   //En el enum
	};

	//todo estado necesitara las funcionalidades
	//basicas de los estados de juego
	virtual void insertCoin();
	virtual void pressButton();
	virtual void moveStick(sf::Vector2i direction);
	virtual void Update(sf::Time delta);
	virtual void Draw(sf::RenderWindow& window);

	bool display_text;
	private:
		Game* m_game;
		
		
	
};
/*Cada uno de los estados tendra su implementacion 
de los metodos de la clase GameState*/
/************************************************/
//Maquina de estados finito
//Estado de no coin
class NoCoinState : public GameState
{
	public:
	NoCoinState(Game* game);

	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void Update(sf::Time delta);
	void Draw(sf::RenderWindow& window);

	private:
		sf::Text m_text;
		sf::Sprite m_sprite;
		

};
/*****************/
//GetReady
class GetReadyState : public GameState
{
public:
	GetReadyState(Game* game,GameState*m_playingState);
	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void Update(sf::Time delta);
	void Draw(sf::RenderWindow& window);
private:
	sf::Text m_text;
	GameState* m_playingState;
};

/***************************/

class PlayingState : public GameState
{
public:
	PlayingState(Game* game);

	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void Update(sf::Time delta);
	void Draw(sf::RenderWindow& window);

	//cuando se pasa un nivel se va al siguiente(0-1-2)
	void loadNextLevel();

	//cuando llegue al ultimo nivel reinicia y vuelve al nivel 0
	void resetToZero();


	void resetCurrentLevel();
	void resetLiveCount();

	void updateCamera();//update positon of camera for scroll
	void moveCharactersToInitialPosition();
	
	~PlayingState();
private:
	sf::Text m_text;
	PacWoman* m_pacWoman;
	std::vector<Ghost*> m_ghosts;
	Maze m_Maze;

	//declarando la camara
	sf::View m_camera;

	//para el hub del jugador
	sf::RenderTexture m_scene;

	//para el texto del hub
	sf::Text m_scoreText;
	sf::Text m_levelText;
	sf::Text m_remainingDotsText;
	sf::Sprite m_livesSprite[3];
public:
	//varialble para el control de niveles
	int m_level;

	//varialbe para el control de vida
	int m_liveCount;

	//varialble para el control de puntos
	int m_score;

};

/***************************/
//LostState
class LostState : public GameState
{
public:
	LostState(Game* game , GameState* playingState);
	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void Update(sf::Time delta);
	void Draw(sf::RenderWindow& window);
private:
	sf::Text m_text;
	sf::Time m_countDown;
	sf::Text m_countDownText;

	//lo necesitamos para acceder a los valores de playing state 
	// y asi poder reinciar
	PlayingState* m_playingState;
};

class WonState : public GameState
{
public:
	WonState(Game* game,GameState* playingState);
	void insertCoin();
	void pressButton();
	void moveStick(sf::Vector2i direction);
	void Update(sf::Time delta);
	void Draw(sf::RenderWindow& window);
private:
	sf::Text m_text;
	PlayingState* m_playingState;
};

#endif  //PACWOMAN_GAMESTATE_HPP





