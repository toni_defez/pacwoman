#ifndef PACWOMAN_GAME_HPP
#define PACWOMAN_GAME_HPP


#include <SFML\Graphics.hpp>
#include <iostream>
#include "GameState.h"
#include <array>

class Game
{
	private:
		sf::RenderWindow m_window;
		GameState* m_currentState;
		//el array indica el tipo y despues la cantidad
		std::array<GameState*, GameState::Count> m_gameStates;
		sf::Font m_font;
		sf::Texture m_logo;
		sf::Texture m_texture;
		
	public:
		Game();
		~Game();   
		void Run();
		//getters 
		sf::Font& getFont();
		sf::Texture& getLogo();
		sf::Texture& getTexture();

		//cambiador de estado de nivel
		void ChangeGameState(GameState::State gameState);
};

#endif //PACWOMAN_GAME_HPP

 